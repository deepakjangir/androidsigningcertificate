var exec = require('cordova/exec');

exports.getSigningCertificate = function (arg0, success, error) {
    exec(success, error, 'AndroidSigningCertificate', 'getSigningCertificate', [arg0]);
};
