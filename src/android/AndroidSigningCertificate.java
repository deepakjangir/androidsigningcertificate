package cordova.plugin.signingCertificate;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import java.security.MessageDigest;

/**
 * This class echoes a string called from JavaScript.
 */
public class AndroidSigningCertificate extends CordovaPlugin {
    private static final int VALID = 0;

    private static final int INVALID = 1;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("getSigningCertificate")) {
            String message = args.getString(0);
            this.getSigningCertificate(message, callbackContext);
            return true;
        }
        return false;
    }

    public int getSigningCertificate(String message, CallbackContext callbackContext) {
        Context context = cordova.getActivity().getApplicationContext();

        try {

            PackageInfo packageInfo = context.getPackageManager()

                    .getPackageInfo(context.getPackageName(),

                            PackageManager.GET_SIGNATURES);

            for (Signature signature : packageInfo.signatures) {

                byte[] signatureBytes = signature.toByteArray();

                MessageDigest md = MessageDigest.getInstance("SHA");

                md.update(signatureBytes);

                final String currentSignature = Base64.encodeToString(md.digest(), Base64.DEFAULT);

                // compare signatures

                if (message.equals(currentSignature)) {
                    callbackContext.success(VALID);
                    return VALID;

                } else {
                    callbackContext.success(INVALID);
                    return INVALID;
                }
                ;

            }

        } catch (Exception e) {
            callbackContext.error(e.toString());
            // assumes an issue in checking signature., but we let the caller decide on what
            // to do.

        }

        return INVALID;
    }
}
